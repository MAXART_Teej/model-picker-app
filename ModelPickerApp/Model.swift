//
//  model.swift
//  ModelPickerApp
//
//  Created by Tharanga Kasthuriarachchi on 14/7/20.
//  Copyright © 2020 Tharanga Kasthuriarachchi. All rights reserved.
//

import UIKit
import RealityKit
import Combine

class Model {
    var modelName: String
    var image: UIImage
    var modelEntity: ModelEntity?
    
    private var cancellable: AnyCancellable? = nil
    
    init(modelName: String) {
        self.modelName =  modelName
        self.image = UIImage(named: modelName)!
        
        let fileName = modelName + ".usdz"
        self.cancellable = ModelEntity.loadModelAsync(named: fileName)
            .sink(receiveCompletion: { loadCompletion in
                // Handle Error
                print("DEBUG: Unable to load modelEntity for modelName: \(self.modelName)")
            }, receiveValue: { modelEntity in
                // Get Model Enity
                self.modelEntity = modelEntity
                print("DEBUG: Succesfully loaded modelEntity for modelName: \(self.modelName)")
            })
        
    }
}
